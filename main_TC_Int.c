/*
 * File:   main_TC_Int.c
 * Author:         MICROCHIP
 * Processor:      PIC18f4520
 * Reference:      http://microchip.wikidot.com/faq:31
   Complier:       XC8 v1.00 or higher 
 * Created on September 19, 2016, 1:01 AM
 */
#include <xc.h>
 
int tick_count=0x0;
 
void main(void)
{
    T1CON = 0x01;               //Configure Timer1 interrupt
    PIE1bits.TMR1IE = 1;           
    INTCONbits.PEIE = 1;
    RCONbits.IPEN=0x01;
    IPR1bits.TMR1IP=0x01;            // TMR1 high priority ,TMR1 Overflow Interrupt Priority bit
    INTCONbits.GIE = 1;
    PIR1bits.TMR1IF = 0;
    T0CON=0X00;
    INTCONbits.T0IE = 1;               // Enable interrupt on TMR0 overflow
    INTCON2bits.TMR0IP=0x00;        
    T0CONbits.TMR0ON = 1;
    while (1);
}
 
void interrupt tc_int(void)             // High priority interrupt
{
    if (TMR1IE && TMR1IF)
    {
        TMR1IF=0;
        ++tick_count;
        TRISC=1;
        LATCbits.LATC7 = 0x01;
    }
}
 
void interrupt low_priority   LowIsr(void)    //Low priority interrupt
{
    if(INTCONbits.T0IF && INTCONbits.T0IE)  // If Timer flag is set & Interrupt is enabled
    {                               
        TMR0 -= 250;                    // Reload the timer - 250uS per interrupt
        INTCONbits.T0IF = 0;            // Clear the interrupt flag 
        ADCON1=0x0F;
        TRISB=0x0CF;
        LATBbits.LATB5 = 0x01;          // Toggle a bit 
    }
    if (TMR1IE && TMR1IF)
    {
        TMR1IF=0;
        ++tick_count;
        TRISC=0;
        LATCbits.LATC6 = 0x01;
    }
}